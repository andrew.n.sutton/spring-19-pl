# spring-19-pl

Programming languages

Each subdirectory contains a language implementation. Each language is
defined by the sets of expressions it provides, denoted by various
abbreviations, which are as follows:

- u -- the language is untyped
- t -- the language is typed
- l -- the language supports lambda expressions
- b -- the language supports boolean operations
- n -- the language supports numeric operations
- r -- the language supports references
- d -- the language supports data abstraction
- f -- system f -- extend typed lambda calculus + universal and existential types
- w -- type constructors

For example, the language `tlbn` is the simply typed lambda calculus extended
with boolean and numeric operations.

Note that lambda expressions generally include multi-argument lambdas and
(more traditional) function calls, but may also include basic single-argument
lambda abstractions. We generally assume multi-argument lambdas are closed.

In general the `b` language is required for the `n` language.

Note that the `r` language does not implement small step semantics.

TODO: It might be useful to merge all of the arithmetic extension (bn) into
The simply typed lambda calculus, since that's not otherwise a particularly
interesting language.

TODO: Support unit types in the set of fundamental types and sequencing!

TODO: Basically, rebuild this whole system (and FIX it).

## Language order

We discussed languages in the following order:

1. ub -- A simple boolean calculator language
2. ul -- The pure untyped lambda calculus with facilities for church encodings.
3. ulb -- This implementation includes big step semantics for lambdas.
4. tbn -- A typed calculator language.
5. tlbn -- A typed calculator language with lambdas.
6. tlbnr -- A typed calculator language with references
7. tlbnrd -- A typed calculator lots of stuff
8. lw -- A typed lambda calculus with type operators
9. f -- System F: typed lambda calculus with parametric polymorphism
10. fw -- System Fw: System F + type operators

